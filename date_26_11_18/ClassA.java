package com.git.date_26_11_18;

public class ClassA {
	int nr;
	String str;
	char chr;
	boolean isTrue;
	ExecutorClass obj;
	
	ClassA(){
		System.out.println("Constructor fara parametri de intrare");
	}
	
	ClassA(String str){
		this.str = str;
	}
	
	ClassA(String str, int nr){
		this(str);
		this.nr = nr;		
	}
	
	ClassA(String str, int nr, char chr){
		this(str, nr);
		this.chr = chr;
	}
	
	ClassA(String str, int nr, char chr, boolean isTrue){
		this(str, nr, chr);
		this.isTrue = isTrue;
	}
	
	public void salutare() {
		System.out.println("Hello world");
	}
	
	public void salutare(String text) {
		System.out.println(text);
	}
	
	public Class<? extends ClassA> getCurrentClass() {
		return this.getClass();
	}
	
	public void initializeSTR(ClassA obj) {
		obj.str = "buna";
	}
	
	public void setInt(int nr) {
		ClassA obj = new ClassA();
		obj.initializeSTR(obj);
	}
	
	public void sayHelloWorld() {
		this.salutare();
	}
	
	

}
