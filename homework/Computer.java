package com.git.homework;

import java.util.Random;

/**
 * @author Valeriu
 *
 */

public class Computer {
	String user;
	int password;
	int encryptedPass;
	String mesajLogare;
	boolean ok;
	
	/**
	 * Metoda care are ca parametru de intrare numele user-ului
	 * @param user numele user
	 * @return Returneaza un mesaj care indica cu care user ai fost logat
	 */
	public String login(String user) {
		 
		switch (user) {		
		case "guest":
			user = "guest";
			break;
		case "user":
			user="user";
			break;
		default: 
			user = "unknown";
		}
		return mesajLogare = "Te-ai logat cu '" + user + "'";
	}
	
	/**
	 * Metoda ce arata parola codificata
	 */
	public void showEncrypedPassword(){
		System.out.println("Parola codificata este = " + encryptedPass);
		
	}
	
	
	/**
	 * Metoda ce codifica parola dupa numarul primit la input
	 * @param pass parametru de intrare
	 */
	public void encryptPassword(int pass){
		Random random = new Random();
		for(int i=0; i<pass; i++) {
			encryptedPass+=random.nextInt(900);
			if(i==pass)
			{
				break;
			}
		}		
	}

}
