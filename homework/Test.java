package com.git.homework;

public class Test {

	public static void main(String[] args) {
		Computer pc = new Computer();
		
		pc.login("user");
		System.out.println(pc.mesajLogare);
		
		pc.login("Valeriu");
		System.out.println(pc.mesajLogare);
		
		pc.showEncrypedPassword();
		
		pc.encryptPassword(123);
		pc.showEncrypedPassword();
		
		System.out.println(pc.encryptedPass);

	}

}
