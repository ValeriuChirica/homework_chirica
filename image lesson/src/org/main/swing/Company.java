package org.main.swing;

import javax.swing.ImageIcon;

public class Company {
	int id;
	String name;
	ImageIcon img;

	public ImageIcon getImg() {
		return img;
	}

	public void setImg(ImageIcon img) {
		this.img = img;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + "]";
	}

	public Company(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Company(int id, String name, ImageIcon img) {
		super();
		this.id = id;
		this.name = name;
		this.img = img;
	}

}
