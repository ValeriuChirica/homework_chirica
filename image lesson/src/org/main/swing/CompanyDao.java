package org.main.swing;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

public class CompanyDao {

	public List<Company> compList = new ArrayList<>();


	public List<Company> getCompList() {
		return compList;
	}

	public void addCompany(Company company) {
		compList.add(company);
	}

	public Company update(Company compUpdate) {
		findById(compUpdate.id).name = compUpdate.name;
		return null;
	}
	
	public void updateImage(int id, ImageIcon img) {
		findById(id).img = img;
	}

	public boolean deleteById(int id) {
		return compList.remove(findById(id));

	}

	public Company findById(int id) {
		for (Company comp : compList) {
			if (comp.id == id) {
				return comp;
			}
		}
		return null;
	}
	
	
}