package org.main.swing;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.NumberFormat;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.NumberFormatter;
import java.awt.Toolkit;

public class CompanyEx extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3962647973391851554L;
	private JPanel contentPane;
	private JFormattedTextField txtId;
	private JTextField txtName;
	private JTable table;
	DefaultTableModel model;
	CompanyDao compUtility;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompanyEx frame = new CompanyEx();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompanyEx() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(CompanyEx.class.getResource("/org/main/swing/source/settings.png")));
		initialize();
		compUtility = new CompanyDao();
		model = (DefaultTableModel) table.getModel();
		model.setColumnIdentifiers(new Object[] { "ID", "NAME" });
	}

	private void initialize() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		setTitle("Company (Tekwill)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 593, 433);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Settings", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setLayout(null);

		NumberFormat format = NumberFormat.getInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(0);
		formatter.setMaximum(Integer.MAX_VALUE);
		formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of focus lost
		formatter.setCommitsOnValidEdit(true);
		txtId = new JFormattedTextField();
		txtId.setBounds(62, 16, 132, 20);
		panel_1.add(txtId);
		txtId.setHorizontalAlignment(SwingConstants.CENTER);
		txtId.setColumns(10);

		txtName = new JTextField();
		txtName.setBounds(62, 47, 132, 20);
		panel_1.add(txtName);
		txtName.setHorizontalAlignment(SwingConstants.CENTER);
		txtName.setColumns(10);

		JLabel lblNewLabel = new JLabel("ID :");
		lblNewLabel.setBounds(6, 19, 46, 14);
		panel_1.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("NAME : ");
		lblNewLabel_1.setBounds(6, 50, 46, 14);
		panel_1.add(lblNewLabel_1);

		JButton btnAdd = new JButton("ADD");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNewCompany();
			}
		});
		btnAdd.setBounds(6, 84, 89, 23);
		panel_1.add(btnAdd);

		JButton btnDelete = new JButton("DEL BY ID");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteCompany();
			}
		});
		btnDelete.setBounds(6, 152, 89, 23);
		panel_1.add(btnDelete);

		JButton btnUpdate = new JButton("UPDT NAME");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNameCompany();
			}
		});
		btnUpdate.setBounds(6, 118, 89, 23);
		panel_1.add(btnUpdate);

		JButton btnFind = new JButton("FIND BY ID");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Company comp = findCompany();
				if(comp==null)
					return;
				searchInTable(comp.id);
				txtName.setText(comp.name);
				addImage(comp.img);
			}
		});
		btnFind.setBounds(105, 118, 89, 23);
		panel_1.add(btnFind);

		JButton btnAddImage = new JButton("ADD IMAGE");
		btnAddImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addImageToExisting();
			}
		});
		btnAddImage.setBounds(105, 84, 89, 23);
		panel_1.add(btnAddImage);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "ImageIcon", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		
		JScrollPane scrollPane_1 = new JScrollPane();

		label = new JLabel("");
		scrollPane_1.setViewportView(label);
		label.setBackground(Color.WHITE);
		label.setOpaque(true);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
					.addGap(4))
		);
		panel.setLayout(gl_panel);

		JScrollPane scrollPane = new JScrollPane();

		table = new JTable();
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent event) {
				txtId.setText(table.getValueAt(table.getSelectedRow(), 0).toString());
				txtName.setText(table.getValueAt(table.getSelectedRow(), 1).toString());
				int id = Integer.valueOf(txtId.getText());
				Company comp = compUtility.findById(id);
				addImage(comp.img);
			}
		});
		scrollPane.setViewportView(table);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(1)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE))
					.addGap(4)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(6)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
							.addGap(9)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE))
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
		);
		contentPane.setLayout(gl_contentPane);

	}

	protected void searchInTable(int id) {
		for (int r = 0; r < model.getRowCount(); r++)
			if (model.getValueAt(r, 0).toString().equals(String.valueOf(id))) {
				table.getSelectionModel().setSelectionInterval(r, r);
				break;
			}

	}

	protected void addNewCompany() {
		if (empty())
			return;
		int id = Integer.valueOf(txtId.getText().trim());
		String name = txtName.getText().trim().toUpperCase();
		ImageIcon img = null;
		if (!unique(id)) {
			JOptionPane.showMessageDialog(null, "Id nu este unic");
			resetFields();
			return;
		}
		

		resetFields();
		int raspuns = JOptionPane.showConfirmDialog(null, "Vrei sa adaugi si poza?", "Poza", JOptionPane.YES_NO_OPTION);
		if (raspuns == 0)
			img = addImage(browse());
		else
			addImage(null);
		
		Company comp = new Company(id, name, img);
		compUtility.addCompany(comp);
		model.addRow(new Object[] { id, name });
	}

	private ImageIcon browse() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setDialogTitle("Alegeti imaginea");
		fileChooser.setCurrentDirectory(new File(System.getProperty("user.home") + "/Desktop/"));
		fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Image files ", "png", "jpg"));

		int rVal = fileChooser.showOpenDialog(null);

		if (rVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			System.out.println(selectedFile.getAbsolutePath());
			ImageIcon img = new ImageIcon(selectedFile.getAbsolutePath());
			return img;
		}
		else
			return null;
	}

	private boolean empty() {
		if (txtId.getText().length() == 0 || txtName.getText().length() == 0)
			return true;
		else
			return false;
	}

	private boolean unique(int id) {
		if (model.getRowCount() == 0)
			return true;
		for (int r = 0; r < model.getRowCount(); r++)
			if (model.getValueAt(r, 0).toString().equals(String.valueOf(id)))
				return false;
		return true;
	}

	private void resetFields() {
		txtId.setText("");
		txtName.setText("");
	}

	protected void deleteCompany() {
		if (empty())
			return;
		int id = Integer.valueOf(txtId.getText().trim());
		compUtility.deleteById(id);
		Integer rows = model.getRowCount();
		for (int r = 0; r < rows; r++)
			if (model.getValueAt(r, 0).toString().equals(String.valueOf(id))) {
				model.removeRow(r);
				break;
			}

		resetFields();
	}

	protected void updateNameCompany() {
		if (empty())
			return;
		int id = Integer.valueOf(txtId.getText().trim());
		String name = txtName.getText().trim().toUpperCase();
		ImageIcon img = null;
		compUtility.update(new Company(id, name, img));
		for (int r = 0; r < model.getRowCount(); r++)
			if (model.getValueAt(r, 0).toString().equals(String.valueOf(id))) {
				model.setValueAt(name, r, 1);
				break;
			}
	}

	protected Company findCompany() {
		if (empty())
			return null;
		int id = Integer.valueOf(txtId.getText().trim());
		return compUtility.findById(id);
	}

	private ImageIcon addImage(ImageIcon img) {
		label.setIcon(img);
		return img;
	}
	
	private void addImageToExisting() {
		if (empty())
			return;
		ImageIcon img = browse();
		compUtility.updateImage(Integer.valueOf(txtId.getText().trim()), img);
		addImage(img);
	}

}
